#!/usr/bin/env python
import sys

BOTS = ['B', 'O']

def load_testcases(filename):
    testcases = []
    with open(filename, 'r') as f:
        T = int(f.readline().strip())
        for i in range(1, T + 1):
            elem = f.readline().strip().split(' ')
            N = int(elem[0])
            instructions = []
            for b in range(0, N):
                color = elem[b * 2 + 1]
                num = int(elem[b * 2 + 2])
                instructions.append((color, num))
            testcases.append(instructions)
    return testcases

def find_next_button(bot, testcase, current):
    for i in range(current, len(testcase)):
        color, button = testcase[i]
        if color == bot:
            return button
    return None

def find_current_bot(testcase, current):
    return testcase[current][0]

def find_next_pos(testcase, current):
    next_pos = {}
    for bot in BOTS:
        next_pos[bot] = find_next_button(bot, testcase, current)
    return next_pos

def solve(testcase):
    time = 0
    pos = {'B': 1, 'O': 1}
    current = 0

    while current < len(testcase):
        current_bot = find_current_bot(testcase, current)
        next_pos = find_next_pos(testcase, current)

        pushed = False
        for bot in BOTS:
            if bot == current_bot and pos[bot] == next_pos[bot]:
                current += 1
            else:
                if pos[bot] < next_pos[bot]:
                    pos[bot] += 1
                elif pos[bot] > next_pos[bot]:
                    pos[bot] -= 1

        time += 1

    return time

def process(filename):
    testcases = load_testcases(filename)
    count = 1
    for testcase in testcases:
        time = solve(testcase)
        print "Case #%d: %d" % (count, time)
        count += 1

if __name__ == "__main__":
    if len(sys.argv) != 2:
        sys.stderr.write("Usage: bottrust.py filename\n")
        sys.exit(1)
    else:
        process(sys.argv[1])

