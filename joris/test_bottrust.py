import unittest
import bottrust

class TestBotTrust(unittest.TestCase):

    def test_load_testcases(self):
        expected = [[('O', 2), ('B', 1), ('B', 2), ('O', 4)]]
        testcases = bottrust.load_testcases('A-single.in')
        self.assertEquals(expected, testcases)

    def test_tiny(self):
        testcase = [('O', 2), ('B', 1), ('B', 2), ('O', 4)]
        time = bottrust.solve(testcase)
        self.assertEquals(6, time)

    def test_find_next_button(self):
        testcase = [('O', 2), ('B', 1), ('B', 2), ('O', 4)]
        self.assertEquals(2, bottrust.find_next_button('O', testcase, 0))
        self.assertEquals(1, bottrust.find_next_button('B', testcase, 0))

        self.assertEquals(4, bottrust.find_next_button('O', testcase, 1))
        self.assertEquals(1, bottrust.find_next_button('B', testcase, 1))

        self.assertEquals(4, bottrust.find_next_button('O', testcase, 3))
        self.assertEquals(None, bottrust.find_next_button('B', testcase, 3))

    def test_find_current_bot(self):
        testcase = [('O', 2), ('B', 1), ('B', 2), ('O', 4)]
        self.assertEquals('O', bottrust.find_current_bot(testcase, 0))
        self.assertEquals('B', bottrust.find_current_bot(testcase, 1))
