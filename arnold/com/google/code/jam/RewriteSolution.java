package com.google.code.jam;

import com.google.code.jam.util.InputParser;
import com.google.code.jam.util.IntegerList;

public class RewriteSolution{
	// Flags and masks for picking apart instructions / transitions.
	public final static int BLUE_FLAG = 0x80000000;
	public final static int ORANGE_FLAG = 0x40000000;
	private final static int COLOR_MASK = 0xc0000000;
	
	public final static int MOVE_ORDER_FLAG = 0x20000000;
	public final static int PRESS_ORDER_FLAG = 0x10000000;
	private final static int ORDER_MASK = 0x30000000;
	
	private final static int POSITION_MASK = 0x0fffffff;
	
	private RewriteSolution(){
		super();
	}
	
	public static boolean isBlue(int transition){
		return (BLUE_FLAG == (transition & COLOR_MASK));
	}
	
	public static boolean isOrange(int transition){
		return (ORANGE_FLAG == (transition & COLOR_MASK));
	}
	
	public static boolean areDifferentColor(int transition1, int transition2){
		return (((transition1 ^ transition2) & COLOR_MASK) != 0);
	}
	
	public static boolean areSameColor(int transition1, int transition2){
		return (((transition1 ^ transition2) & COLOR_MASK) == 0);
	}
	
	public static boolean isMoveOrder(int transition){
		return (MOVE_ORDER_FLAG == (transition & ORDER_MASK));
	}
	
	public static int getPosition(int transition){
		return (transition & POSITION_MASK);
	}
	
	private static int addMovementTransitions(int order, int color, int location, IntegerList transitions){
		int targetLocation = getPosition(order);
		if(targetLocation > location){ // Forward
			for(int j = location + 1; j <= targetLocation; ++j){
				transitions.add(color | MOVE_ORDER_FLAG | j);
			}
		}else if(targetLocation < location){ // Backward
			for(int j = location - 1; j >= targetLocation; --j){
				transitions.add(color | MOVE_ORDER_FLAG | j);
			}
		}
		return targetLocation;
	}
	
	// Unfold the instruction list into individual transitions
	// i.e. OM4 OP4 BP3 => OM2 OM3 OM4 OP4 BM2 BM3 BP3
	private static int[] unroll(int[] instructions){
		IntegerList transitions = new IntegerList();
		
		int blueLocation = 1;
		int orangeLocation = 1;
		for(int i = 0; i < instructions.length; ++i){
			int instruction = instructions[i];
			if(isBlue(instruction)){ // Blue
				blueLocation = addMovementTransitions(instruction, BLUE_FLAG, blueLocation, transitions);
				transitions.add(BLUE_FLAG | PRESS_ORDER_FLAG | blueLocation); // Press button at the current location
			}else if(isOrange(instruction)){ // Orange
				orangeLocation = addMovementTransitions(instruction, ORANGE_FLAG, orangeLocation, transitions);
				transitions.add(ORANGE_FLAG | PRESS_ORDER_FLAG | orangeLocation); // Press button at the current location
			}
		}
		
		return transitions.getContent(); // The list of transitions
	}
	
	// Reorder the list to the most optimal order
	// Rules:
	// Presses can't be moved across each other
	// Moves should be pulled forward as much as possible
	// i.e.
	// B* BM OM => B* OM BM
	// B* BP OM => B* OM BP
	// O* OM BM => O* BM OM
	// O* OP BM => O* BM OP
	// Note: These only work when applied from the start
	private static void optimize(int[] transitions){
		for(int i = 2; i < transitions.length;){
			int transition = transitions[i];
			int previousTransition = transitions[i - 1];
			int beforePreviousTransition = transitions[i - 2];
			
			if(isMoveOrder(transition) &
					areSameColor(beforePreviousTransition, previousTransition) &
					areDifferentColor(previousTransition, transition)){
				transitions[i - 1] = transition;
				transitions[i] = previousTransition;
				
				// A change occurred; re-check the affected 'zone'
				if(i >= 4) i -= 2;
				else i = 2;
			} else ++i; // No change occurred; move to the next 'zone'
		}
	}
	
	// Cluster orange + blue moves
	// Cluster orange move + blue press
	// Cluster blue move + orange press
	// Count number of clusters (== the solution)
	private static int calculate(int[] transitions){
		int iterations = 0;
		int nrOfTransitions = transitions.length;
		int i;
		for(i = 0; i < nrOfTransitions - 1; ++i){ // All but the last
			++iterations;
			
			int transition = transitions[i];
			int nextTransition = transitions[i + 1];
			if(areDifferentColor(transition, nextTransition) &
					(isMoveOrder(transition) | isMoveOrder(nextTransition))){
				++i; // Can be done in parallel, so consume / skip over the next one
			}
		}
		if(i == nrOfTransitions - 1){ // If one transition remains at the end, add it
			++iterations;
		}
		
		return iterations;
	}
	
	public static int solve(int[] instructions){
		int[] transitions = unroll(instructions);
		optimize(transitions);
		return calculate(transitions);
	}
	
	
	public static void main(String[] args){
		String[] sequences = InputParser.readFromStdIn();
		int[][] instructionsList = InputParser.parseSequences(sequences);
		
		for(int i = 0; i < instructionsList.length; ++i){
			int result = RewriteSolution.solve(instructionsList[i]);
			System.out.print("Case #");
			System.out.print(i + 1);
			System.out.print(": ");
			System.out.println(result);
		}
	}
}
