package com.google.code.jam.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.google.code.jam.RewriteSolution;

public class InputParser{
	
	private InputParser(){
		super();
	}
	
	public static int[] parseSequence(String sequence){
		String[] parts = sequence.split(" ");
		int nrOfParts = parts.length;
		int[] instructions = new int[(nrOfParts - 1) >> 1];
		for(int i = 1; i < nrOfParts - 1; i += 2){
			int instruction = "O".equals(parts[i]) ? RewriteSolution.ORANGE_FLAG : RewriteSolution.BLUE_FLAG;
			instruction |= RewriteSolution.PRESS_ORDER_FLAG;
			instruction |= Integer.parseInt(parts[i + 1]);
			
			instructions[i >> 1] = instruction;
		}
		return instructions;
	}
	
	public static int[][] parseSequences(String[] sequences){
		int numberOfLines = sequences.length;
		int[][] instructionsList = new int[numberOfLines][];
		for(int i = 0; i < numberOfLines; ++i){
			instructionsList[i] = parseSequence(sequences[i]);
		}
		return instructionsList;
	}
	
	public static String[] readFromStdIn(){
		BufferedReader reader = null;
		try{
			reader = new BufferedReader(new InputStreamReader(System.in));
			String line;
			if((line = reader.readLine()) != null){
				int inputLines = Integer.parseInt(line);
				String[] input = new String[inputLines];
				
				while(((line = reader.readLine()) != null) && (--inputLines >= 0)){
					input[input.length - inputLines - 1] = line;
				}
				return input;
			}
			return new String[0];
		}catch(IOException ex){
			throw new RuntimeException(ex);
		}finally{
			if(reader != null){
				try{
					reader.close();
				}catch(IOException ex){
					throw new RuntimeException(ex);
				}
			}
		}
	}
}
