package com.google.code.jam.util.debug;

import com.google.code.jam.RewriteSolution;

public class TransitionPrinter{
	
	private TransitionPrinter(){
		super();
	}
	
	public static void printTransition(int transition){
		if(RewriteSolution.isBlue(transition)){
			if(RewriteSolution.isMoveOrder(transition)){
				System.out.print("B M ");
			}else{
				System.out.print("B P ");
			}
		}else{
			if(RewriteSolution.isMoveOrder(transition)){
				System.out.print("O M ");
			}else{
				System.out.print("O P ");
			}
		}
		System.out.print(RewriteSolution.getPosition(transition));
	}
	
	public static void printTransitions(int[] transitions){
		for(int i = 0; i < transitions.length; ++i){
			printTransition(transitions[i]);
			System.out.print(" ");
		}
		System.out.println();
	}
}
