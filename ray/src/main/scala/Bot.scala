import io.Source
import java.io.{BufferedWriter, FileWriter}
import math._

object BotThrust extends App {
  val fw = new BufferedWriter(new FileWriter(args(1)))
  try Bot.process(args(0)).foreach{ line =>
    fw.write(line)
    fw.newLine()
  } finally fw.close()
}

object Bot {
  def process(filename:String): Seq[String] = {
    parse(filename).map { commands =>
      val (orangeBot, blueBot) = mkBots(commands)
      val seq = sequence(commands)
      trackTime(orangeBot, blueBot, seq)
    }.foldLeft(List[String]()) { (acc, time) =>
      acc :+ "Case #%d: %d".format(acc.size + 1,time)
    }
  }

  def parse(filename: String): Seq[Seq[(Int, Int)]] = {
    val source = Source.fromFile(filename)
    val lines = source.mkString.split('\n').toList
    lines.tail.map {
      line =>
        val commands = line.split(' ').toList.tail.sliding(2, 2).flatMap {
          case List("O", n) => Some((1, Integer.parseInt(n)))
          case List("B", n) => Some((2, Integer.parseInt(n)))
          case _ => None
        }
        commands.toList
    }
  }

  def sequence(commands: Seq[(Int, Int)]): Seq[Int] = {
    commands.map(_._1)
  }

  def mkBots(commands: Seq[(Int, Int)]): (Bot, Bot) = {
    val list = List(1, 2).map {
      color => commands.filter(_._1 == color).map(_._2).foldLeft((List[Int](), 1)) {
        case ((list, pos), c) => (list :+ (abs(c - pos) + 1), c)
      }._1
    }
    (Bot(1, 0, list(0)), Bot(2, 0, list(1)))
  }

  def trackTime(orange: Bot, blue: Bot, sequence: Seq[Int]): Int = {
    if (orange.stepsRemaining.isEmpty || blue.stepsRemaining.isEmpty) {
      orange.stepsRemaining.sum + blue.stepsRemaining.sum
    } else {
      val (orangeBot, blueBot) = recurseSteps(orange, blue, sequence)
      max(orangeBot.time, blueBot.time)
    }
  }

  def recurseSteps(orangeBot: Bot, blueBot: Bot, sequence: Seq[Int]): (Bot, Bot) = {
    val optionalNextOrange = orangeBot.stepsRemaining.headOption
    val optionalNextBlue = blueBot.stepsRemaining.headOption
    List(optionalNextOrange, optionalNextBlue).flatten match {
      case Nil =>
        (orangeBot, blueBot)
      case _ =>
        val (newOrange, newBlue) = sequence.headOption.map {
          case 1 =>
            val newOrange = orangeBot.doSteps()
            (newOrange, blueBot.doSteps(notMyTurn = true, newOrange.pressedButton))
          case 2 =>
            val newBlue = blueBot.doSteps()
            (orangeBot.doSteps(notMyTurn = true, newBlue.pressedButton), newBlue)
        }.getOrElse((orangeBot.finish, blueBot.finish))

        if (!sequence.isEmpty) {
          val newSeq = if (newOrange.pressedButton || newBlue.pressedButton) sequence.tail else sequence
          recurseSteps(newOrange, newBlue, newSeq)
        } else {
          (orangeBot.finish, blueBot.finish)
        }
    }
  }
}

case class Bot(color: Int, time: Int = 0, stepsRemaining: List[Int], pressedButton: Boolean = false) {
  def doSteps(notMyTurn: Boolean = false, otherPressed: Boolean = false): Bot = {
    val myTurn = !notMyTurn
    def mkNewSteps(step: Int): List[Int] = if (step == 1) stepsRemaining.tail else (step - 1) :: stepsRemaining.tail
    def wantToPress(step: Int) = step == 1
    def spendTime = time + 1
    stepsRemaining.headOption.map {
      step =>
        val newBot = if (wantToPress(step) && (notMyTurn || otherPressed)) {
          Bot(color, spendTime, stepsRemaining)
        } else if (wantToPress(step) && myTurn) {
          Bot(color, spendTime, mkNewSteps(step), pressedButton = true)
        } else if (!wantToPress(step)) {
          Bot(color, spendTime, mkNewSteps(step))
        } else {
          Bot(color, spendTime, stepsRemaining)
        }
        newBot
    }.getOrElse(this)
  }

  def finish = {
    Bot(color, time + stepsRemaining.sum, List(), pressedButton = true)
  }
}