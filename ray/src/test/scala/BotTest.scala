import org.scalatest.matchers.MustMatchers
import org.scalatest.WordSpec

class BotTest extends WordSpec with MustMatchers {
  "O 2 B 1 B 2 O 4 " must {
    "return 6" in {
      Bot.trackTime(Bot(1,0,List(2,3)),Bot(2,0,List(1,2)),List(1,2,2,1)) must be (6)
    }
  }

  "O 5 O 8 B 100" must {
    "return 100" in {
      Bot.trackTime(Bot(1,0,List(5,4)),Bot(2,0,List(100)),List(1,1,2)) must be (100)
    }
  }

  "B 2 B 1" must {
    "return 4" in {
      Bot.trackTime(Bot(1,0,List()),Bot(2,0,List(2,2)),List(2,2)) must be (4)
    }
  }
}
