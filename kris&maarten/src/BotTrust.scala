
object BotColor extends Enumeration {
  type BotColor = Value
  val O, B = Value
}

case class Order(val bot: BotColor.Value, val button: Int)

case class BotTrustProblem(orders: List[Order])

case class Move(val bot: BotColor.Value, val steps: Int)

class BotPosition(private var position: Int = 1) {
  def move(o: Order) = {
    val m = new Move(o.bot, Math.abs(position - o.button) + 1)
    position = o.button
    m
  }
}

class GameTimes(val lastOrangeTime: Int = 0, val lastBlueTime: Int = 0) {
  private def doMove(steps: Int, toMove: Int, other: Int) = (toMove + steps).max(other + 1)
  def withMove(move: Move) = move.bot match {
    case BotColor.O => new GameTimes(doMove(move.steps, lastOrangeTime, lastBlueTime), lastBlueTime)
    case BotColor.B => new GameTimes(lastOrangeTime, doMove(move.steps, lastBlueTime, lastOrangeTime))
  }
}

object BotTrust extends GoogleCodeJamSolver[BotTrustProblem] {
  def parseProblems(lines: List[String]) = lines.tail.map(line => BotTrustProblem(line.split(" ").toList.tail.sliding(2, 2).map(_ match { case bot :: button :: Nil => new Order(BotColor.withName(bot), button.toInt) }).toList));
  def solve(problem: BotTrustProblem) = {
    val orange = new BotPosition()
    val blue = new BotPosition()
    val moves = problem.orders.map(o => o.bot match {
      case BotColor.O => orange.move(o)
      case BotColor.B => blue.move(o)
    })
    val endTimes = moves.foldLeft(new GameTimes())((times, move) => {
      times.withMove(move)
    })
    endTimes.lastBlueTime.max(endTimes.lastOrangeTime).toString();
  }

}