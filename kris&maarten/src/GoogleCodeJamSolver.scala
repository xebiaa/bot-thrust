abstract class GoogleCodeJamSolver[T] {
  import scala.io._
  import java.io._

  def main(args: Array[String]): Unit = {
    val in = new File(args.first);
    val out = new File(in.getParentFile(), in.getName().replace(".in", ".out"))
    val source = Source.fromFile(in);
    val target = new PrintWriter(new FileWriter(out));
    var lines = source.getLines().toList
    source.close();
    for ((problem, index) <- parseProblems(lines).zipWithIndex) {
      val line = "Case #" + (index + 1) + ": " + solve(problem)
      println(line)
      target.println(line)
    }
    target.close();
  }
  
  def parseProblems(lines: List[String]): List[T];
  
  def solve(problem: T): String;
  
}

